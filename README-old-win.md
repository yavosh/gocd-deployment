## III. Agent image creation

*Windows agent is configured manually.*

Rationale: We could invest more time to automate the agent installation via 
ansible however to enable a windows image for remote management we still need to
have a custom image which allows remote management (powershell and winrm) enabled.
Since that step is unavoidable adding a small extra step of installing the agent
seems like a good compromise.

I am not aware of windows provisioning tools that allow to provision a windows
machine automatically. Therefore I have decided to create a windows image manually
with the goal being to make the image available for reuse and multiple instances 
can be started from the base image. Provisioning a machine with linux should 
be easier using packer in a similar fashion as the server.

Here are the steps followed to provision the windows image manually.

### a) Create a new EC2 instance using the windows 2012 base image. 

### b) setup powershell scripts

    dism.exe /online /enable-feature /featurename:MicrosoftWindowsPowerShellRoot
    dism.exe /online /enable-feature /featurename:MicrosoftWindowsPowerShell /all
    
    powershell Set-ExecutionPolicy RemoteSigned

    powershell

    Add-WindowsFeature Server-Gui-Shell, Server-Gui-Mgmt-Infra

### c) preform ansible prep tasks

    See instructions here
    http://docs.ansible.com/intro_windows.html
    we need to enable windows remote management and upgrade powershell 3

### d) create management user

create a user ansible and give them administrator privilleges. Username and 
password should be added to ```ansible/group_vars/win_agents.yml``` for future 
management tasks.


### e) download and install gocd agent 

    http://download.go.cd/gocd/go-agent-14.4.0-1356-setup.exe
    bitsadmin /transfer mydownloadjob  /download /priority normal http://download.go.cd/gocd/go-agent-14.4.0-1356-setup.exe c:\go-agent-14.4.0-1356-setup.exe

    or pwoershell

    pwoershell

    wget http://download.go.cd/gocd/go-agent-14.4.0-1356-setup.exe -OutFile c:\go-agent-14.4.0-1356-setup.exe



Install the agent on the machine and provide the following hostname for the server 
```go-server.int.example.com```

    go-agent-14.4.0-1356-setup.exe /S /SERVERIP=go-server.int.example.com

The hostname will be configured in an internal AWS Route 53 zone to make it easier to 
change the server address in the future.