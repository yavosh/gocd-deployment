# Setup gocd server and windows agent

## I. Pre-Requisites

All operations are tested on OSX. All instructions should also work on linux.

### Software
For following software must be installed on the provisioning machine

 * packer (used version Packer v0.7.5)
 * ansible (ansible 1.8.2)
 * packer-windows-plugins (v1.0.0-rc) 
   install pre-built binaries https://github.com/packer-community/packer-windows-plugins/releases
   https://github.com/packer-community/packer-windows-plugins


### AWS working account

You will need have a working aws account

### AWS Keys

Create AWS user for api access and downlod AWS api keys.
Setup the environment variables AWS_ACCESS_KEY & AWS_SECRET_KEY

Setup once or add to bashrc/zshrc

    # For packer and ec2 tools
    export AWS_ACCESS_KEY=''
    export AWS_SECRET_KEY=''

    # For ansible the variables are slightly different, ansible uses boto
    export AWS_ACCESS_KEY_ID=''
    export AWS_SECRET_ACCESS_KEY=''

### The 5 minute guide

    # checkout scripts (this repository)
    git clone https://yavosh@bitbucket.org/yavosh/gocd-deployment.git
    cd ./gocd-deployment

    # Set vpc-id and vpc-subnet-id variables for packer to work
    vi ./scripts/vars.sh

    # Create a route 53 zone called example.com 
    # you can change the name if you like, make sure the correct name is 
    # defined in ansible variables)

    # Create server image
    ./scripts/build-gocd-server.sh
    # Record the created server ami-id using the command output

    # Create server image
    ./scripts/build-gocd-agent.sh
    # Record the created server ami-id using the command output

    # edit ansible variables and provide correct values for all variables defined
    # make sure to use the server ami id and agent ami id from previus steps
    vi ./ansible/group_vars/localhost.yml

    # Let the magic happen
    cd ansible
    ansible-playbook -i ./hosts playbooks/task_setup.yml -vvvv

    # After some time the agents should connect to the server


## II. Server image creation

Rationale: We could opt to go with a plain ubuntu image and use ansible to 
configure the software and configuration. By instead creating a ready pre-built 
image we have faster time to spin up new images and more predictable results.

A server image is created using packer. Image is provisioned using "amazon-ebs"
builder and stored as an AMI image. 

You have to configure the following variables in ```scripts/vars.sh```

    export AWS_VPC_ID=""
    export AWS_SUBNET_ID=""


To create the image run the included script

    $ ./scripts/build-gocd-server.sh

The script create an AMI image and will install the gocd server (version 14.4.0).
Please record the AMI-ID of the created image.

	1429913726,,ui,say,==> amazon-ebs: Stopping the source instance...
	1429913727,,ui,say,==> amazon-ebs: Waiting for the instance to stop...
	1429913782,,ui,say,==> amazon-ebs: Creating the AMI: gocd server 1429913518
	1429913783,,ui,message,    amazon-ebs: [AMI: ami-85adc0f2]
	1429913783,,ui,say,==> amazon-ebs: Waiting for AMI to become ready...

In the above example the AMI-ID is **[AMI: ami-85adc0f2]**


Problems: In one case the built script complained about missing package "default-jre-headless"
not sure what was the problem there putting it here in case it happens again to 
investigate.

## III. (automatic) Agent image creation

Notes: This method relies on packer plugins which are not considered stable.
borrows heavily from https://github.com/dylanmei/packer-windows-templates

Windows image will be created using packer windows plugin
https://github.com/packer-community/packer-windows-plugins

You have to configure the following variables in ```scripts/vars.sh```

    export AWS_VPC_ID=""
    export AWS_SUBNET_ID=""

To create the image run the included script

    $ ./scripts/build-gocd-agent.sh

The script create an AMI image and will install the gocd server (version 14.4.0).
Please record the AMI-ID of the created image.

    1430092639,amazon-windows-ebs,artifact-count,1
    1430092639,amazon-windows-ebs,artifact,0,builder-id,mitchellh.amazonebs
    1430092639,amazon-windows-ebs,artifact,0,id,eu-west-1:ami-53016d24
    1430092639,amazon-windows-ebs,artifact,0,string,AMIs were created:\n\neu-west-1: ami-53016d24
    1430092639,amazon-windows-ebs,artifact,0,files-count,0
    1430092639,amazon-windows-ebs,artifact,0,end
    1430092639,,ui,say,--> amazon-windows-ebs: AMIs were created:\n\neu-west-1: ami-53016d24


In the above example the AMI-ID is **[AMI: ami-53016d24]**


### III. (alternative) Agent image creation manual steps

*Windows agent is configured manually.*

Here are the steps followed to provision the windows image manually.

### a) Create a new EC2 instance using the windows 2012 base image. 

### b) setup powershell scripts

    dism.exe /online /enable-feature /featurename:MicrosoftWindowsPowerShellRoot
    dism.exe /online /enable-feature /featurename:MicrosoftWindowsPowerShell /all
    
    powershell Set-ExecutionPolicy RemoteSigned

    powershell Add-WindowsFeature Server-Gui-Shell, Server-Gui-Mgmt-Infra

### c) preform ansible prep tasks

    See instructions here
    http://docs.ansible.com/intro_windows.html
    we need to enable windows remote management and upgrade powershell 3

### d) create management user

create a user ansible and give them administrator privilleges. Username and 
password should be added to ```ansible/group_vars/win_agents.yml``` for future 
management tasks.


### e) download and install gocd agent 

	http://download.go.cd/gocd/go-agent-14.4.0-1356-setup.exe
    bitsadmin /transfer mydownloadjob  /download /priority normal http://download.go.cd/gocd/go-agent-14.4.0-1356-setup.exe c:\go-agent-14.4.0-1356-setup.exe

    or pwoershell

    pwoershell

    wget http://download.go.cd/gocd/go-agent-14.4.0-1356-setup.exe -OutFile c:\go-agent-14.4.0-1356-setup.exe



Install the agent on the machine and provide the following hostname for the server 
```go-server.int.example.com```

	go-agent-14.4.0-1356-setup.exe /S /SERVERIP=go-server.int.example.com

The hostname will be configured in an internal AWS Route 53 zone to make it easier to 
change the server address in the future.



## IV. DNS Setup

Configure a private DNS zone using route 53
Setup and a record to point ```go-server.int.example.com``` to the private ip of the
gocd server (ip will be updated when the server is created using ansible)

After the ip is created the agents will appear in the gocd server agents screen.


## V. Provision instances with ansible

To spin up instances an ansible playbook will be used.
The playbook ```ansible/playbooks/task_setup.yml``` will execute the following
steps.


### Create server 

Spin up a single gocd server instance using the previously created ami image 
(step II )

### Setup server in route53 

Associate the private ip address of the server with the route 53 host 
go-server.int.example.com.

### Spin up a number of gocd agent instances 

Using the pre built windows image (step III) we can run a number of agent 
instances by setting the ansible variable ```agent_instances_count``` in 
```ansible/group_vars/localhost.yml```

The playbook is idempotent and rerunning the playbook will not spin-up new
instances. If you need to add or remove agents change the variable to the 
desired number and run the playbook again. Ansible is responsible for spinning 
up new instances or destroying them to match the desired number of agents.

