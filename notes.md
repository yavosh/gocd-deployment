
### ENV Setup 

#### The following enviroment varaibles must be set

# Default vars
export AWS_ACCESS_KEY=...
export AWS_SECRET_KEY=...

# For BOTO / Ansible
export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY}
export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_KEY}

### Ansible Setup
sudo pip install boto

### Notes

When there is no default VPC both subnet_id and vpc_id must be supplied 
when creating the image

Use something like this to capture the ami-id after packer build
https://gist.github.com/irgeek/2f5bb964e3ce298e15b7