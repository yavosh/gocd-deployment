#!/usr/bin/env bash

## This script will be run by packer to provision the AWS image

set -e

echo "*** SERVER PROVISIONING"
## Install all updates
echo "*** UPDATE PACKAGES"
sudo apt-get update -y -qq 
sudo apt-get upgrade -y -qq 

## Install gocd dependencies
echo "*** INSTALL DEPS"
sudo apt-get install unzip -y -qq

sleep 5

# sudo apt-get install default-jre-headless -y -qq
# sudo apt-get install default-jre -y -qq
# sudo apt-get install default-jre -y -qq
sudo apt-get install openjdk-7-jre -y -qq

## Download gocd server 
echo "*** DOWNLOAD GO SERVER"
curl -Lo /tmp/go-server-14.4.0-1356.deb http://download.go.cd/gocd-deb/go-server-14.4.0-1356.deb

## Install gocd server
echo "*** INSTALL GO SERVER"
sudo dpkg -i /tmp/go-server-14.4.0-1356.deb

## Remove package
echo "*** REMOVE GO SERVER PACKAGE"
sudo rm /tmp/go-server-14.4.0-1356.deb

## Give some time for server to start
sleep 15

## Check status
echo "*** CHECK STATUS"
sudo /etc/init.d/go-server status

