#!/usr/bin/env bash

## Create an AWS EC2 image based on ubuntu and install gocd and dependencies

source ./scripts/vars.sh

packer build -machine-readable \
    -var "aws_access_key=${AWS_ACCESS_KEY}" \
    -var "aws_secret_key=${AWS_SECRET_KEY}" \
    -var "aws_vpc_id=${AWS_VPC_ID}" \
    -var "aws_subnet_id=${AWS_SUBNET_ID}" \
    packer/gocd-server.json | tee build.log

# Extract ami id of created image
grep 'artifact,0,id' build.log | cut -d, -f6 | cut -d: -f2 > server-ami-id.txt


