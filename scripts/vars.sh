#!/usr/bin/env bash

## Setup environment variables here

export AWS_VPC_ID="vpc-73468a16"
export AWS_SUBNET_ID="subnet-818a35e4"

### Scripts will need changes if region is changed ami-id will have to be changed
export AWS_REGION="eu-west-1"
